package com.nestia.oom;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by genlei.peng on 17/1/9.
 *
 * -Xms20m -Xmx20m  -verbose:gc -XX:+PrintGCDateStamps -XX:+PrintGCDetails
 */
public class HeapOOM {

    static class OOMObject {

    }

    public static void main(String[] args) {
        List<OOMObject> oomObjectList = Lists.newArrayList();

        while (true) {
            oomObjectList.add(new OOMObject());
        }
        //will print
        //Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
    }

}
