package com.nestia.oom;

/**
 * Created by genlei.peng on 17/1/9.
 * -Xss258k
 */
public class JavaStackSOF {
    public static void main(String[] args) {
        JavaStackSOF javaStackSOF = new JavaStackSOF();
        javaStackSOF.stackLeak();
    }

    private int stackLength = 1;

    public void stackLeak() {
        stackLength++;
        System.out.println(stackLength);
        stackLeak();
    }

    //will print
    //Exception in thread "main" java.lang.StackOverflowError

}
