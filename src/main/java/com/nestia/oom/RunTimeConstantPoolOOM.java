package com.nestia.oom;

import com.google.common.collect.Lists;

import java.util.List;

/**
 * Created by genlei.peng on 17/1/9.
 * -Xmx20m
 */
public class RunTimeConstantPoolOOM {
    public static void main(String[] args) {
        List<String> list = Lists.newArrayList();
        long i = 0;

        while (true) {
            String s = String.valueOf(i++).intern();
            list.add(s);
            System.out.println(s);
        }
    }
    //will print Exception in thread "main" java.lang.OutOfMemoryError: Java heap space
    //because there is no Perm in jvm8,and RunTimeConstantPool moved to heap in jvm7
}
